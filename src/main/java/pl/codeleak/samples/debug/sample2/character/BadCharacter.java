package pl.codeleak.samples.debug.sample2.character;

public class BadCharacter extends Character {

    public BadCharacter(final String name, final int energy) {
        super(name, energy);
    }

}
